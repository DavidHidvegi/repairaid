import './App.css';
import { useState } from 'react'

const getQueryAnswer = async query => {
  const url = "https://api.repairaid.davidhidvegi.com"

  const resp = await fetch(url, {
    method: "POST",
    body: JSON.stringify({ query }),
    headers: { 'Content-type': 'application/json' }
  })
  
  return resp.json()
}

function App() {
  const [query, setQuery] = useState("")
  const [history, setHistory] = useState([{id: 0, sender:"repairaid", text: "Hi, I'm RepairAid, a conversational program repair chatbot, how can I help you?"}])

  const updateQuery = evt => setQuery(evt.target.value)

  const keyPressed = key => {
    if(key.keyCode === 13){
      submitQuery()
    }
  }

  const resolveQuery = async (query) => {
    const response = await getQueryAnswer(query)
    setHistory(lastHistory => [...lastHistory, {id: lastHistory.length, sender:"repairaid", text: response.answer}])
  }

  const submitQuery = async () => {
    resolveQuery(query)
    setHistory(lastHistory => [...lastHistory, {id: lastHistory.length, sender:"client", text: query}])
    setQuery("")
  }

  return (
    <div className="App">
      <header className="App-header">
      <h3>Check the repos here: </h3>
      <p>ReactJS Frontend <a href="https://gitlab.com/DavidHidvegi/repairaid">GitHub Repo</a>, Cloudflare Worker Backend <a href="https://gitlab.com/DavidHidvegi/repairaid-worker">GitHub Repo</a></p>
      <p>Language library nlp.js library <a href="https://github.com/axa-group/nlp.js/">GitHub Repo</a>, Master Thesis <a href="https://www.overleaf.com/read/jqdghfrwhhkc">Overleaf PDF</a></p>

      
      {history.map(message => message.sender === "client" ?
        <p key={message.id}> {message.text} </p>:
        <h3 key={message.id}> {message.text} </h3>
      )}
      <input id="query" type="textarea" onChange={updateQuery} value={query} onKeyDown={keyPressed} placeholder="Search query" />
      <button onClick={submitQuery}>Search</button>
      </header>
    </div>
  );
}

export default App;
